# Medical NER

## Project Execution
Run ./setup.sh to start the docker image of the API.         
Input a config json file with required models and a text file to be tagged.

| key     | value 
| -----   | -------- 
| config  | config file     
| data    | input data file

> sample config and input data are present in the example folder

## Description
The API consists of 5 models(4 scispacy models and 1 medacy model) trained on medical and clinical data.

### Scispacy models

| Name                     | model                | entity list
| -------------            | -------              | -----------
| biomedical ontologies    | en_ner_craft_md      | GGP, SO(Sequence Ontology), TAXON(Taxonomy), CHEBI(Chemical Entities of Biological Interest), GO(Gene Ontology), CL
| bio-entity recognition   | en_ner_jnlpba_md     | DNA, CELL_TYPE, CELL_LINE, RNA, PROTEIN
| chemical disease relation| en_ner_bc5cdr_md     | DISEASE, CHEMICAL
| knowledge based bio-nlp  | en_ner_bionlp13cg_md | AMINO_ACID, ANATOMICAL_SYSTEM, CANCER, CELL, CELLULAR_COMPONENT, DEVELOPING_ANATOMICAL_STRUCTURE, GENE_OR_GENE_PRODUCT, IMMATERIAL_ANATOMICAL_ENTITY, MULTI-TISSUE_STRUCTURE, ORGAN, ORGANISM, ORGANISM_SUBDIVISION, ORGANISM_SUBSTANCE, PATHOLOGICAL_FORMATION, SIMPLE_CHEMICAL, TISSUE

### Medacy model

Model trained on patient clinical notes with entities:
Drug, Strength, Duration, Route, Form, ADE, Dosage, Reason, Frequency

## Sample 

For a sample input text: "I've tried a few antidepressants over the years (citalopram, fluoxetine, amitriptyline), but none of those helped with my depression, insomnia, anxiety."
Here is what the models return:
```json
{
    "bio-entity recognition": [
        {
            "entities": [
                {
                    "end": "1",
                    "label": "PROTEIN",
                    "phrase": "I",
                    "start": "0"
                }
            ],
            "sentence": "I've tried a few antidepressants over the years (citalopram, fluoxetine, amitriptyline), but none of those helped with my depression, insomnia, anxiety."
        }
    ],
    "biomedical ontologies": [
        {
            "entities": [
                {
                    "end": "13",
                    "label": "CHEBI",
                    "phrase": "fluoxetine",
                    "start": "12"
                },
                {
                    "end": "15",
                    "label": "CHEBI",
                    "phrase": "amitriptyline",
                    "start": "14"
                }
            ],
            "sentence": "I've tried a few antidepressants over the years (citalopram, fluoxetine, amitriptyline), but none of those helped with my depression, insomnia, anxiety."
        }
    ],
    "chemical disease relation": [
        {
            "entities": [
                {
                    "end": "6",
                    "label": "CHEMICAL",
                    "phrase": "antidepressants",
                    "start": "5"
                },
                {
                    "end": "11",
                    "label": "CHEMICAL",
                    "phrase": "citalopram",
                    "start": "10"
                },
                {
                    "end": "13",
                    "label": "CHEMICAL",
                    "phrase": "fluoxetine",
                    "start": "12"
                },
                {
                    "end": "15",
                    "label": "CHEMICAL",
                    "phrase": "amitriptyline",
                    "start": "14"
                },
                {
                    "end": "25",
                    "label": "DISEASE",
                    "phrase": "depression",
                    "start": "24"
                },
                {
                    "end": "27",
                    "label": "DISEASE",
                    "phrase": "insomnia",
                    "start": "26"
                },
                {
                    "end": "29",
                    "label": "DISEASE",
                    "phrase": "anxiety",
                    "start": "28"
                }
            ],
            "sentence": "I've tried a few antidepressants over the years (citalopram, fluoxetine, amitriptyline), but none of those helped with my depression, insomnia, anxiety."
        }
    ],
    "clinical notes": [
        {
            "entities": [
                {
                    "end": 9,
                    "label": "Drug",
                    "phrase": "citalopram",
                    "start": 9
                },
                {
                    "end": 10,
                    "label": "Drug",
                    "phrase": "fluoxetine",
                    "start": 9
                },
                {
                    "end": 11,
                    "label": "Drug",
                    "phrase": "amitriptyline",
                    "start": 10
                }
            ],
            "sentence": "I've tried a few antidepressants over the years (citalopram, fluoxetine, amitriptyline), but none of those helped with my depression, insomnia, anxiety."
        }
    ],
    "knowledge based bio-nlp": [
        {
            "entities": [
                {
                    "end": "11",
                    "label": "SIMPLE_CHEMICAL",
                    "phrase": "citalopram",
                    "start": "10"
                },
                {
                    "end": "13",
                    "label": "SIMPLE_CHEMICAL",
                    "phrase": "fluoxetine",
                    "start": "12"
                },
                {
                    "end": "15",
                    "label": "SIMPLE_CHEMICAL",
                    "phrase": "amitriptyline",
                    "start": "14"
                }
            ],
            "sentence": "I've tried a few antidepressants over the years (citalopram, fluoxetine, amitriptyline), but none of those helped with my depression, insomnia, anxiety."
        }
    ]
}
```

