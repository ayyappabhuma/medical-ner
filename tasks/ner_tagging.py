from utils import celery_instance
from celery.exceptions import Ignore
from tasks.tasker import taskize, TaskUpdates
import re

import spacy


def medacy_clinical_notes(inp_data):

    from medacy.model.model import Model
#Loading the pre-trained model
    model = Model.load_external('medacy_model_clinical_notes')
#Loading English sentence splitter, parser.
    nlp = spacy.load("en_core_web_sm")
    dataset_file = open(inp_data,'r')
    data = dataset_file.read()
#Sentence splitter
    sentences = nlp(data)
    processed_sentences = []
#Iterating over the text on sentence level
    for sent in sentences.sents:
        sentence_dict = {}
        entities_list = []
        annotation = model.predict(sent.text)
        if len(annotation) != 0:
            for item in annotation:
                span_dict = {}
#Using regex to take out the relevant info, i.e, phrase , label , start character index , end character index of the identified phrase
                z = re.match("EntTuple\((.*)\)",str(item))
                if z:
                    values = (z.group(1)).split(',')
                    for value in values:
                        value = value.strip()
                        st_x = re.match("start=(\w+)",value)
                        if st_x:
                            st_id = st_x.group(1)
                        en_x = re.match("end=(\w+)",value)
                        if en_x:
                            en_id = en_x.group(1)
                        phrase_x = re.match("text=\'((\w+)( +\w+)*)\'",value)
                        if phrase_x:
                            phrase = phrase_x.group(1)
                        label_x = re.match("tag=\'((\w+)( +\w+)*)\'",value)
                        if label_x:
                            label = label_x.group(1)
#Counting the start and end token indices of the identified phrase
                sent_st = sent.text[0:int(st_id)]
                sent_en = sent.text[0:int(en_id)]
                token_list_st = sent_st.split()
                token_list_en = sent_en.split()
#Adding the phrase , start , end indices of the phrase and its label to a dictionary
                span_dict['start'] = len(token_list_st)
                span_dict['end'] = len(token_list_en)
                span_dict['phrase'] = phrase
                span_dict['label'] = label
#Adding the created dictionary item into a list that will have all the entities present in a sentence
                entities_list.append(span_dict)
#Adding the sentence and its entities list to a dictionary sentence_dict
        sentence_dict['sentence'] = sent.text.strip()
        sentence_dict['entities'] = entities_list
#All the sentence_dict are put into a list
        if entities_list:
            processed_sentences.append(sentence_dict)
    return(processed_sentences)


# Function for scispacy models on medical NER
def scispacy_models(inp_data, model_name):
    nlp = spacy.load(model_name)
    with open(inp_data, "r") as f:
        data = f.readlines()
    processed_sentences = []
    for line in data:
        line = line.strip()
        sentences = nlp(line)

        for sent in sentences.sents:
            entities_list = []
            lines = nlp(sent.text)
            for ent in lines.ents:
                span_dict = {'start': str(ent.start), 'end': str(ent.end), 'phrase': str(ent), 'label': str(ent.label_)}
                entities_list.append(span_dict)

            if entities_list:
                sentence_dict = {'sentence': sent.text.strip(), 'entities': entities_list}
                processed_sentences.append(sentence_dict)
    return processed_sentences


@celery_instance.task(bind=True)
@taskize
def tags(tasker: TaskUpdates, config, input_data):
    tasker.update_status(state=tasker.states.STARTED, message={'message': 'test'})
    try:
        req_models = config["required_models"]
        scispacy_model_names = {"biomedical ontologies": "en_ner_craft_md", "bio-entity recognition": "en_ner_jnlpba_md",
                                "chemical disease relation": "en_ner_bc5cdr_md", "knowledge based bio-nlp": "en_ner_bionlp13cg_md"
                                }
        output_dict = {}
   
        for model in req_models:
            if model == "clinical notes":
                output_dict["clinical notes"] = medacy_clinical_notes(input_data)

            elif str(model) in scispacy_model_names.keys():
               output_dict[model] = scispacy_models(input_data, scispacy_model_names[model])

        message = {'task_id': tasker.task_id, 'config': tasker.config,
                   'message': f'annotating'
                   }

        tasker.update_status(state=tasker.states.STARTED, message=message)

    except Exception as exp:
        tasker.update_status(state=tasker.states.FAILURE, message={'error': str(exp)})
        raise Ignore()
    else:
        message['final_output'] = output_dict
        tasker.update_status(state=tasker.states.SUCCESS, message=message)
        return message
