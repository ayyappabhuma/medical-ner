import pytest
from medical_ner_api import flask_app


@pytest.fixture(scope='module')
def client():
    flask_app.config['DEBUG'] = True
    flask_app.config['TESTING'] = True
    with flask_app.test_client() as c:
        yield c


@pytest.fixture(scope='session')
def celery_config():
    return {
        'broker_url': 'amqp://',
        'result_backend': 'redis://'
    }


def test_add(celery_worker):
    mytask.delay()
