import connexion
from flask import redirect, abort, json, jsonify
import tempfile

from celery.signals import before_task_publish, after_task_publish
from tasks import states as task_states
from utils import celery_instance
from tasks.ner_tagging import tags


@before_task_publish.connect
def before_task_sent_handler(sender=None, headers=None, body=None, **kwargs):
    """
    This function is executed after a task is published/queued. The primary purpose of this code is to ensure that the
    status of the task when submitted is changed from the default 'PENDING' to 'RECEIVED'. This is because non existent
    task ids also return status as 'PENDING'
    :param sender:
    :param headers:
    :param body:
    :param kwargs:
    :return:
    """

    info = headers if 'task' in headers else body
    task = celery_instance.tasks.get(sender)
    backend = task.backend if task else celery_instance.backend
    backend.store_result(info['id'], None, task_states.RECEIVED)
    len(kwargs)


@after_task_publish.connect
def after_task_sent_handler(sender=None, headers=None, body=None, **kwargs):
    """
    This function is executed after a task is published/queued. The primary purpose of this code is to ensure that the
    status of the task when submitted is changed from the default 'RECEIVED' to 'QUEUED'. This is because non existent
    task ids also return status as 'PENDING'
    :param sender:
    :param headers:
    :param body:
    :param kwargs:
    :return:
    """

    info = headers if 'task' in headers else body
    task = celery_instance.tasks.get(sender)
    backend = task.backend if task else celery_instance.backend
    backend.store_result(info['id'], None, task_states.QUEUED)
    len(kwargs)


def post():
    """
    This functions processes the post request for the sleep api
    :return: Redirect URL for results
    """
    if 'config' not in connexion.request.files:
        return "Missing config parameters in POST request", 400

    if 'data' not in connexion.request.files:
        return "Missing data file in POST request", 400

    tracker = connexion.request.form.get('tracker', None)

    try:
        conf = json.load(connexion.request.files['config'])
        data = connexion.request.files['data']

    except Exception as exp:
        return abort(400, f"Exception: {exp}")

    if data is not None:
        try:
            with tempfile.TemporaryDirectory() as tmpdirname:
                data.save(tmpdirname + data.filename)
                result = tags.delay(conf, tmpdirname + data.filename, tracker=tracker)

        except Exception as exp:
            return abort(400, f"Error occurred: {exp}")
        else:
            return redirect(f"{connexion.request.url}/{result.id}", code=303)
    else:
        return abort(400, f"Invalid data.")


def get(task_id):
    """
    Get task status and results
    :param task_id:
    :return:
    """

    task = tags.AsyncResult(task_id)

    response = {
        'state': task.state,
        'status': task.info
    }
    if task.state == task_states.PENDING:
        # job did not start yet
        response['status'] = 'Task not present or deleted'
        return jsonify(response), 404

    if task.state == task_states.QUEUED:
        # job did not start yet
        return jsonify(response), 202
    elif (task.state == task_states.STARTED) or (task.state == task_states.SUCCESS):
        response = {
            'state': task.state,
            'status': task.info
        }
        if 'result' in task.info:
            response['result'] = task.info['result']

        if task.state == task_states.STARTED:
            return jsonify(response), 202
        else:
            return jsonify(task.info['final_output']), 200
    else:
        # something went wrong in the background job
        response = {
            'state': str(task.state),
            'current': 1,
            'total': 1,
            'status': str(task.info),  # this is the exception raised
        }
        return jsonify(response), 404


def delete(task_id):
    """
    Revoke a queued task or delete results from a processed task
    :param task_id:
    :return:
    """
    print(task_id)
    celery_instance.control.revoke(task_id)
    response = {'message': ''}
    try:
        task = tags.AsyncResult(task_id)

        if task.state == task_states.QUEUED:
            task.revoke()
            return f'{task_id} is scheduled for deletion:', 200
        elif task.state == task_states.STARTED:
            return f'{task_id} is currently being processed. Cannot be preempted', 409
        elif task.state == task_states.SUCCESS:
            backend = task.backend if task else celery_instance.backend
            data = task.info
            # data['status'].pop('message')
            backend.store_result(task_id, data, task_states.DELETED)
            """
            TODO: Need to update task id status with deleted tag instead of merely purging results. However should
            suffice for now
            """
            return f'Task has already been processed. Deleting results for {task_id}', 200
        elif task.state == task_states.DELETED:
            return f'{task_id} has been deleted', 410
        elif task.state == task_states.PENDING:
            return f'{task_id} is not present or has been deleted', 404

    except Exception as exp:
        response = {
            'task_id': task_id,
            'error': str(exp)
        }

    return jsonify(response)
