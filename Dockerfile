FROM ubuntu:18.04

WORKDIR /
RUN apt-get update
RUN apt-get install python3 python3-pip -y
RUN apt-get install python3.7 python3.7-dev -y
RUN python3.7 -m pip install pip
RUN apt-get install git -y

WORKDIR /medical_ner_api/
COPY requirements.txt /medical_ner_api/
RUN python3.7 -m pip install -r requirements.txt
COPY medacy_model.txt /medical_ner_api/
RUN python3.7 -m spacy download en_core_web_sm
RUN python3.7 -m pip install -r medacy_model.txt
COPY scispacy_models.txt /medical_ner_api/
RUN python3.7 -m pip install -r scispacy_models.txt

WORKDIR /medical_ner_api/
COPY . /medical_ner_api/

RUN touch app.log
RUN touch backend.log
